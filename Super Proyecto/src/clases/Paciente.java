package clases;

public class Paciente extends Persona implements Comparable { 
	
	/**@class clase Paciente
	 * @brief Datos del Paciente
	 * @author Ismael Burgos
	 * @date 2018/05/01  
	 */    
	
	  
	// ATRIBUTOS DE PACIENTE
	
	private String telefono;
	private String dni;
	private Enfermedad enfermedad; 
	private Tratamiento tratamiento;    
	
	
	// CONSTRUCTOR DE PACIENTE
	
	public Paciente(String n, String a,String tel, String dni, Enfermedad enf) { 
		super(n, a);
		telefono = tel;
		this.dni = dni;
		this.enfermedad = enf;
		tratamiento=null; 
	}
	
	// GETTERS Y SETTERS DE PACIENTE

	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String tel) {
		telefono = tel;
	}


	public String getDni() {
		return dni;
	}


	public void setDni(String dni) {
		this.dni = dni;
	}


	public Enfermedad getEnfermedad() {
		return enfermedad;
	}


	public void setEnfermedad(Enfermedad enfermedad) {
		this.enfermedad = enfermedad; 
	}
	
	
	// METODO ENTRAR HOSPITAL (void)  
	
	public void entrarHospital() {
		
		System.out.println("Pacientes entrando al Hospital\n"); 
	} 
	
	// RECIBE EL TRATAMIENTO DEL PACIENTE
	
	public void recibirTratamiento(Tratamiento t) {
		tratamiento=t;    
	}
	
	// METODO COMPARE TO 

	@Override
	public int compareTo(Object arg0) {
		
		Paciente aux=(Paciente)arg0;
			
		if(this.getEnfermedad().getNombre().equals("resfriado") && (aux.getEnfermedad().getNombre().equals("pulmon�a")||aux.getEnfermedad().getNombre().equals("hernia")||aux.getEnfermedad().getNombre().equals("pata rota"))) {

			return 1;
		}
		
		if(this.getEnfermedad().getNombre().equals("pulmon�a") && aux.getEnfermedad().getNombre().equals("resfriado")) {
			
			return -1;
		}
		
		if(this.getEnfermedad().getNombre().equals("pulmon�a")&&(aux.getEnfermedad().getNombre().equals("hernia")||aux.getEnfermedad().getNombre().equals("pata rota"))) {
			
			return 1;
		}
		
	
		if(this.getEnfermedad().getNombre().equals("hernia") && (aux.getEnfermedad().getNombre().equals("resfriado")||aux.getEnfermedad().getNombre().equals("pulmon�a"))) {
			
			return -1;
			
		}
		
		if(this.getEnfermedad().getNombre().equals("hernia") &&(aux.getEnfermedad().getNombre().equals("pata rota"))) {

			return 1;
		}
		
		
		
		if(this.getEnfermedad().getNombre().equals("pata rota")&&(aux.getEnfermedad().getNombre().equals("resfriado")||aux.getEnfermedad().getNombre().equals("pulmon�a")||aux.getEnfermedad().getNombre().equals("hernia"))) {

			return -1;
			
		}

		return 0;  
	

	}
	
	
	// METODO PARA MOSTRAR LOS PACIENTES
	
	@Override
	public String toString() {
		
		return "Paciente:[: Nombre : = "+getNombre()+ ": Apellidos : ="+ getApellidos() +": tel�fono : =" + telefono + " : dni : =" + dni + " : Con enfermedad: =" + enfermedad + " :Con tratamiento: =:"
				+ tratamiento + "]\n";               
	}   
	
	
	
	

}
