package clases;

import java.util.Random;

public class Enfermedad { 

	/**@class clase Enfermedad
	 * @brief Enfermedad del Paciente
	 * @author Ismael Burgos
	 * @date 2018/05/01   
	 */     


	// ATRIBUTOS DE ENFERMEDAD

	private String nombre;
	private String descripcion; 
	private int gravedad; 


	// CONSTRUCTOR DE ENFERMEDAD

	public Enfermedad(String nomb, String d, int grav) {
		super();
		nombre = nomb;
		descripcion = d;
		this.gravedad=grav;
	}

	// CONSTRUCTOR DE RANGO ALEATORIO DE LA GRAVEDAD DE LA ENFERMEDAD 

	public Enfermedad(String nombre, String d) {

		Random aleatorio=new Random(System.currentTimeMillis()); 

		if(nombre=="Resfriado") {

			this.gravedad=aleatorio.nextInt(21);

		}else if(nombre=="Hernia") {

			this.gravedad=aleatorio.nextInt(40-20)+20;

		}else if(nombre=="Pulmonia") {

			this.gravedad=aleatorio.nextInt(81-40)+40;

		}else if(nombre=="Pata Rota") {

			this.gravedad=aleatorio.nextInt(101-80)+80;  
		}

	}

	// GETTERS Y SETTERS

	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nomb) {
		nombre = nomb;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descr) {
		descripcion = descr; 
	}
	
	public int getGravedad() {
		return gravedad;
	}	

	// METODO MOSTRAR ENFERMEDAD 

	public String mostrarEnfermedad() {

		String desc=("---- ENFERMEDAD ----\n"); 

		desc+="Nombre: " +this.nombre+"\n";
		desc+="Descripción:"+this.descripcion+"\n"; 

		return desc;    
 
	}
	
	// METODO TO STRING ENFERMEDAD 

	@Override
	public String toString() {
		return "Enfermedad [nombre=" + nombre + ", descripcion=" + descripcion + ", gravedad=" + gravedad + "]";  
	}

	


	
	

}
