package clases;

public class Persona { 
	
	/**@class clase Persona
	 * @brief Datos de la Persona
	 * @author Ismael Burgos
	 * @date 2018/05/01  
	 */   
	
	
	// ATRIBUTOS DE PERSONA
	
	private String Nombre;
	private String Apellidos; 
	
	
	// CONSTRUCTOR DE PERSONA
	
	public Persona(String nombre, String apellidos) { 
		super();
		Nombre = nombre; 
		Apellidos = apellidos; 
	}

	
	
	// GETTERS Y SETTERS DE PERSONA

	public String getNombre() {
		return Nombre;
	}


	public void setNombre(String nombre) {
		Nombre = nombre;
	}


	public String getApellidos() {
		return Apellidos;
	}


	public void setApellidos(String apellidos) {
		Apellidos = apellidos; 
	}  
	  	
  
}
