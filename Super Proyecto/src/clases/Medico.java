package clases;

import java.time.LocalDate;
import java.util.HashMap;

public class Medico extends Persona { 

	/**@class clase Medico
	 * @brief El medico del paciente
	 * @author Ismael Burgos
	 * @date 2018/05/01  
	 */ 


	// ATRIBUTOS DEL MEDICO

	Hospital h; 
	HashMap<String,Tratamiento> tratamientosPosibles;  


	// CONSTRUCTOR DEL MEDICO 

	public Medico(String nombre, String apellidos, Hospital ho) {
		super(nombre, apellidos);
		tratamientosPosibles=new HashMap<String,Tratamiento>();
		tratamientosPosibles.put("resfriado", new Tratamiento("Pastillas","Paracetamol"));
		tratamientosPosibles.put("hernia", new Tratamiento("Cirugia","Quirofano de Hernias"));
		tratamientosPosibles.put("pulmon�a", new Tratamiento("Antibiotico","Pulmosol"));
		tratamientosPosibles.put("pata pota", new Tratamiento("Crema anti lesiones","Trombocil"));
		this.h=ho;    

	}


	// METODO DAR GRAVEDAD    


	public int darGravedad(Paciente p) {
		return p.getEnfermedad().getGravedad();	 
	}  

	// METODO TRATAR PACIENTE 

	public String tratarPaciente(Paciente p) {

		if(p.getEnfermedad().getGravedad()<50) {
			p.getEnfermedad().getNombre();
			Tratamiento t=tratamientosPosibles.get(p.getEnfermedad().getNombre()); 
			p.recibirTratamiento(t);  	
			return "El paciente se ha ido con tratamiento";	
		}


		for(Cama i:this.h.getListaCamas()) {

			if(i.estaLibre()) {
				i.setFechaIngreso(LocalDate.now());   
			}

		}
		return "El paciente lo han encamado"; 
		
		

	}

}


