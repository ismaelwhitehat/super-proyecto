package clases;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.Caret;

import com.sun.glass.ui.Window;
import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JTextPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class InterfazHospital extends JFrame {
	
	/** @class InterfazHospital
	 * @brief Clase Interfaz para la aplicaci�n de escritorio del Hospital.
	 * @author Ismael Burgos
	 * @date 2018/06/03 
	 */
	
	// ATRIBUTOS

	private JPanel ContenidoPanel;
	private static Hospital h;
	JLabel labelContadorHospital;
	int contador;     

	
	// CONSTRUCTOR DE LA INTERFAZ HOSPITAL

	/**
	 * Create the frame.
	 */
	public InterfazHospital(Hospital hospital) {
		
		contador=0;   
		h=hospital;
		setBackground(new Color(135, 206, 235));
		setTitle("Hospital");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		ContenidoPanel = new JPanel();
		ContenidoPanel.setBackground(new Color(135, 206, 250));
		ContenidoPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(ContenidoPanel);
		ContenidoPanel.setLayout(null);
		
		
		// PANEL DEL HOSPITAL
		
		JPanel panelHospital = new JPanel();
		panelHospital.setBounds(10, 11, 414, 178);
		ContenidoPanel.add(panelHospital);
		GridBagLayout gbl_panelHospital = new GridBagLayout();
		gbl_panelHospital.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panelHospital.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panelHospital.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panelHospital.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panelHospital.setLayout(gbl_panelHospital);	
		
		// PANEL DE TEXTO DE HOSPITAL
		
		JTextPane PanelTexto = new JTextPane();
		
		GridBagConstraints gbc_PanelTexto = new GridBagConstraints();
		gbc_PanelTexto.gridwidth = 4;
		gbc_PanelTexto.gridheight = 4;
		gbc_PanelTexto.fill = GridBagConstraints.BOTH;
		gbc_PanelTexto.gridx = 0;
		gbc_PanelTexto.gridy = 0;
		panelHospital.add(PanelTexto, gbc_PanelTexto); 	
		
		
		// BOTON SIGUIENTE DE HOSPITAL
		
		JButton BotonSiguiente = new JButton("Siguiente");
		
		BotonSiguiente.addMouseListener(new MouseAdapter() {
			
			
			// ACCI�N CUANDO EL RAT�N SE CLICA DEL BOTON SIGUIENTE
			
			@Override
			public void mouseClicked(MouseEvent arg0) { 
				
				if(BotonSiguiente.isEnabled()) { 
				
					labelContadorHospital.setText((++contador)+"/20");  
					
			        Paciente actual=InterfazHospital.h.getRecepcion().getPacientes().remove(); 
					
					// MEDICO LE ASIGNA UN NUMERO DE GRAVEDAD AL PACIENTE
					
					PanelTexto.setText(actual.getNombre()+", el m�dico "+InterfazHospital.h.getMedico().getNombre()+" dice que la gravedad de tu "+actual.getEnfermedad().getNombre()+" es "+InterfazHospital.h.getMedico().darGravedad(actual)+"\n");
			        
					
					// DAR TRATAMIENTO AL PACIENTE o ENCAMARLO 
					
					PanelTexto.setText(PanelTexto.getText()+(InterfazHospital.h.getMedico().tratarPaciente(actual))+"\n");    
					
					PanelTexto.setText(PanelTexto.getText()+actual.toString()+"\n");
				}
				
			}
		});
		BotonSiguiente.setBounds(165, 211, 138, 23);
		BotonSiguiente.setEnabled(false);
		ContenidoPanel.add(BotonSiguiente);   
		
		
		// BOTON COMENZAR 
		
		JButton BotonComenzar = new JButton("Comenzar"); 
		
		BotonComenzar.addMouseListener(new MouseAdapter() {
			
			
			// ACCI�N PARA CUANDO EL RAT�N EST� CLICADO MUESTRE EN EL PANEL DE TEXTO, UN
			// MENSAJE DE BIENVENIDA
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				PanelTexto.setText("Bienvenidos al Hospital Central 24 hrs, a continuaci�n les mostraremos,"
						+ "la informaci�n de nuestros pacientes, espero que le agraden nuestros servicios,"
						+ "un saludo cordial.");  

				BotonSiguiente.setEnabled(true);
			}
			
		});
		
		
		BotonComenzar.setBounds(31, 202, 101, 40);
		ContenidoPanel.add(BotonComenzar);
		
		
		// BOT�N LABEL CONTADOR PARA QUE CUANDO SE CLIQUE EN SIGUIENTE SE VAYA VIENDO
		// COMO VAN PASANDO LOS PACIENTES DE UNO EN UNO
		
		labelContadorHospital = new JLabel("0/20");
		labelContadorHospital.setLabelFor(labelContadorHospital);
		labelContadorHospital.setBounds(338, 215, 52, 14);
		ContenidoPanel.add(labelContadorHospital);     
		
	} 
	

	} 
