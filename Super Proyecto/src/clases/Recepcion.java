package clases;

import java.util.PriorityQueue;

public class Recepcion { 
	
	/**@class clase Recepcion
	 * @brief Recepcion del hospital
	 * @author Ismael Burgos
	 * @date 2018/05/01      
	 */ 
	
	
	// ATRIBUTO DE RECEPCION (PRIORITY QUEUE)-- COLA
	
	private  PriorityQueue<Paciente> pacientes = new PriorityQueue<Paciente>(); 
	
	
	// GET DE PRIORIDAD AL PACIENTE 

	public PriorityQueue<Paciente> getPacientes(){
		return pacientes;
	}
	
	// A�ADIR PACIENTE A LA COLA DE LA RECEPCION


	public void a�adirPaciente(Paciente p) { 
		pacientes.add(p);
	
	}
	
	// ELIMINAR PACIENTE DE LA COLA DE LA RECEPCION
	
	public void eliminarPaciente(Paciente p) {
		pacientes.remove(p); 
	}
	
	
	// M�TODO DAR PRIORIDAD AL PACIENTE DE LA RECEPCION 
	
	public int darPrioridad(Paciente p) {
		
		if(p.getEnfermedad().getNombre()=="Resfriado") {
			
			return 1;
		
		}else if(p.getEnfermedad().getNombre()=="Hernia") {
			
			return 2;
		
		}else if(p.getEnfermedad().getNombre()=="Pulmonia")	{
			
			return 3;
		
		}else {
			return 4;
		}	
	} 
	
	
	
	// M�TODO RECIBIR PACIENTES DE LA RECEPCI�N
	
	
	public void recibirPacientes() {
		
		System.out.println("----RECEPCI�N----"); 
		System.out.println("Recibiendo pacientes en recepci�n\n");        
		
		
	}
}
