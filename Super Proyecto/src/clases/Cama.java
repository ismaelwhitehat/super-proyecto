package clases;

import java.time.LocalDate;

public class Cama { 
	
	/**@class clase Cama
	 * @brief Las habitaciones de los pacientes
	 * @author Ismael Burgos
	 * @date 2018/05/01   
	 */      
	
	// ATRIBUTOS DE CAMA
	
	private LocalDate fechaIngreso;
	private LocalDate fechaAlta;
	
	// CONSTRUCTOR DE CAMA
	
	public Cama(LocalDate fechaIngreso, LocalDate fechaAlta) {
		super();
		this.fechaIngreso = fechaIngreso;
		this.fechaAlta = fechaAlta; 
	}
	
	// GETTERS Y SETTERS DE CAMA

	public LocalDate getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(LocalDate fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public LocalDate getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;   
	}
	
	// METODO CAMA ESTA LIBRE
	
		public boolean estaLibre() {
			
			if(fechaIngreso!=null) {
				return false;
			}
			return true;
			
		}  
		
		

}
