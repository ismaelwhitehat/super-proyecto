package clases;

import java.util.ArrayList;

public class Hospital { 

	/**@class clase Hospital
	 * @brief Centro Sanitario
	 * @author Ismael Burgos
	 * @date 2018/05/01  
	 */ 
 
	// ATRIBUTO DE HOSPITAL

	private String nombre; 
	private ArrayList<Cama>listaDeCama;
	private Recepcion r; 
	private Medico m; 

	// CONSTRUCTOR DE HOSPITAL

	public Hospital(String nomb) {
		super();
		this.nombre = nomb;
		this.listaDeCama=new ArrayList<Cama>();
		r=new Recepcion();
		m=new Medico("Lolo","Soto",this);
	}



	// GETTERS Y SETTERS DE HOSPITAL


	public String getnombre() {
		return nombre; 
	}
	 


	public void setMostrar(String nombre) {
		this.nombre = nombre;
	}


	// METODO MOSTRAR HOSPITAL 

	public String mostrarHospital() {
		
		System.out.println("---- HOSPITAL ----"); 
		System.out.println("----Central 24 horas----"); 

		String desc="Nombre: " +this.nombre+"\n";  
		return desc;    
		 
	}
	
	
	//  OBTENER LISTA DE CAMAS 
	
	 ArrayList<Cama> getListaCamas(){
		 return this.listaDeCama;   
	 }
	 
	 public Medico getMedico() {
		 return this.m;
	 }
	
	 public Recepcion getRecepcion() {
		 
		 return this.r;  
	 }
}
