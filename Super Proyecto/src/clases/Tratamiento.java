package clases;

public class Tratamiento { 
	
	/**@class clase Tratamiento
	 * @brief Tratamiento de la enfermedad
	 * @author Ismael Burgos
	 * @date 2018/05/01 
	 */   
	 
	
	// ATRIBUTOS DE TRATAMIENTO
	
	 private String EnfermedadTrata;
	 private String Nombre;   
	
	// CONSTRUCTOR DE TRATAMIENTO


	public Tratamiento(String enfermedadTrata, String nombre) {
		EnfermedadTrata = enfermedadTrata;
		Nombre = nombre; 
	}

	
	// GETTERS Y SETTERS DE TRATAMIENTO 

	public String getEnfermedadTrata() {
		return EnfermedadTrata;
	}


	public void setEnfermedadTrata(String enfermedadTrata) {
		EnfermedadTrata = enfermedadTrata;
	}


	public String getNombre() {
		return Nombre;
	}


	public void setNombre(String nombre) {
		Nombre = nombre; 
	} 


	// TO STRING PARA MOSTRAR EL TRATAMIENTO  
	
	@Override
	public String toString() {
		return "Tratamiento [EnfermedadTrata=" + EnfermedadTrata + ", Nombre=" + Nombre + "]";      
	}
	
	
 
}
