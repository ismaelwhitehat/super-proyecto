package Prueba;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import clases.Cama;
import clases.Enfermedad;
import clases.Hospital;
import clases.InterfazHospital;
import clases.Medico;
import clases.Paciente;
import clases.Recepcion;

public class Main {
	
	
	public static void main(String[] args) {
		
		/**
		 * @class Clase de Prueba main
		 * @author Ismael Burgos
		 * @date 2018/06/03 
		 */
		

		//CREACIÓN DEL HOSPITAL

		Hospital hospital=new Hospital("Central 24 hrs");
		hospital.mostrarHospital();  
		

		//CREACIÓN DE 20 PACIENTES CON ENFERMEDAD

		Paciente paciente1=new Paciente("Pepe","Luis","952134567","56348991A",new Enfermedad("resfriado","Con muchos mocos",12)); 
		Paciente paciente2=new Paciente("Ana","Zamora","941267890","89898124Z", new Enfermedad("pulmonía","Encharcamiento de pulmones",20));
		Paciente paciente3=new Paciente("Javier","Exposito","951290456","87129054D", new Enfermedad("hernia","Estirpamiento de hernia",30));
		Paciente paciente4=new Paciente("James","Diaz","956781231","45789120G", new Enfermedad("pata rota","Colocamiento del hueso",40));
		Paciente paciente5=new Paciente("Ian","Villafranca","954789102","67891234L", new Enfermedad("resfriado","Con muchos mocos",12));
		Paciente paciente6=new Paciente("Rodrigo","Dominguez","958912378","89034567S", new Enfermedad("hernia","Estirpamiento de hernia",33));
		Paciente paciente7=new Paciente("Jorge","Marín","958765410","12345693T", new Enfermedad("pulmonía","Encharcamiento de pulmones",29));
		Paciente paciente8=new Paciente("Jesús","Lopez","956789123","56789123F", new Enfermedad("pata rota","Colocamiento del hueso",10));
		Paciente paciente9=new Paciente("André","Segalerva","95367891","89234568H", new Enfermedad("resfriado","Con muchos mocos",34));
		Paciente paciente10=new Paciente("Esteban","Perez","956781923","45678912I", new Enfermedad("hernia","Estirpamiento de hernia",44));
		Paciente paciente11=new Paciente("Pereira","Romero","95678910","67801234J", new Enfermedad("pulmonía","Encharcamiento de pulmones",45));
		Paciente paciente12=new Paciente("Miguel","García","956789104","34567892K", new Enfermedad("pata rota","Colocamiento del hueso",47));
		Paciente paciente13=new Paciente("Sancho","Robinson","978910345","56788012L", new Enfermedad("resfriado","Con muchos mocos",5));
		Paciente paciente14=new Paciente("Peter","Bronckhost","957080803","49596956M", new Enfermedad("hernia","Estirpamiento de hernia",40));
		Paciente paciente15=new Paciente("Luis","Martinez","967891023","67891234N", new Enfermedad("hernia","Estirpamiento de hernia",50));
		Paciente paciente16=new Paciente("María","Dominguez","954678120","12345679O", new Enfermedad("resfriado","Con muchos mocos",20));
		Paciente paciente17=new Paciente("Josefa","Sanchez","95789102","45678912P", new Enfermedad("pulmonía","Encharcamiento de pulmones",60));
		Paciente paciente18=new Paciente("Leire","Ramirez","956789102","69696239Q", new Enfermedad("hernia","Estirpamiento de hernia",30));
		Paciente paciente19=new Paciente("Susana","Martín","95237812","79865423R", new Enfermedad("pata rota","Colocamiento del hueso",10));
		Paciente paciente20=new Paciente("Pablo","Fernandez","956781234","66670609W", new Enfermedad("resfriado","Con muchos mocos",90));  
 
		// INICIO DE RECEPCIÓN Y AÑADIENDO PACIENTES
		
		hospital.getRecepcion().añadirPaciente(paciente1);
		hospital.getRecepcion().añadirPaciente(paciente2);
		hospital.getRecepcion().añadirPaciente(paciente3);
		hospital.getRecepcion().añadirPaciente(paciente4);
		hospital.getRecepcion().añadirPaciente(paciente5);
		hospital.getRecepcion().añadirPaciente(paciente6);
		hospital.getRecepcion().añadirPaciente(paciente7);
		hospital.getRecepcion().añadirPaciente(paciente8);
		hospital.getRecepcion().añadirPaciente(paciente9);
		hospital.getRecepcion().añadirPaciente(paciente10);
		hospital.getRecepcion().añadirPaciente(paciente11); 
		hospital.getRecepcion().añadirPaciente(paciente12);
		hospital.getRecepcion().añadirPaciente(paciente13);
		hospital.getRecepcion().añadirPaciente(paciente14);
		hospital.getRecepcion().añadirPaciente(paciente15);
		hospital.getRecepcion().añadirPaciente(paciente16);
		hospital.getRecepcion().añadirPaciente(paciente17);
		hospital.getRecepcion().añadirPaciente(paciente18);
		hospital.getRecepcion().añadirPaciente(paciente19);
		hospital.getRecepcion().añadirPaciente(paciente20);  
				
		
		// INICIO DE LA APLICACIÓN DE ESCRITORIO (INTERFAZ GRÁFICA) DEL HOSPITAL
		
		InterfazHospital interfaz=new InterfazHospital(hospital);
		interfaz.setVisible(true);  
		
		
		// CONEXIÓN A LA BASE DE DATOS DE HOSPITAL
		
		try {
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Pacientes?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","cenec");
			Statement smt=(Statement) con.createStatement(); 
			
			int nfilas=smt.executeUpdate("create table Pacientes (Nombre varchar(30), Apellidos varchar(30),Telefono int(20)"
					+ "Dni varchar(20), Enfermedad varchar(30), Descripcion varchar(30), Gravedad int(10), Tratamiento varchar(30));"); 
			
			int insercion1=smt.executeUpdate("insert into Pacientes values('James','Diaz',956781231,'45789120G','Pata rota','Colocamiento del hueso',40, null)");
			int insercion2=smt.executeUpdate("insert into Pacientes values('Jesus','Lopez',956789123,'56789123F,'Pata rota','Colocamiento del hueso',10, null)");
			int insercion3=smt.executeUpdate("insert into Pacientes values('Susana','Martin',95237812,'79865423R,'Pata Rota','Colocamiento del hueso',10, null)");
			int insercion4=smt.executeUpdate("insert into Pacientes values('Miguel','Garcia',956789104,'34567892K,'Pata Rota','Colocamiento del hueso',47, null)");
			int insercion5=smt.executeUpdate("insert into Pacientes values('Javier','Exposito',951290456,'87129054D,'Hernia','Estirpamiento de hernia',30,'Cirugia')");
			int insercion6=smt.executeUpdate("insert into Pacientes values('Leire','Ramirez',956789102,'69696239Q,'Hernia','Estirpamiento de hernia',30, 'Cirugia')");
			int insercion7=smt.executeUpdate("insert into Pacientes values('Luis','Martinez',967891023,'67891234N,'Hernia','Estirpamiento de hernia',50, null)");
			int insercion8=smt.executeUpdate("insert into Pacientes values('Esteban','Perez',956781923,'45678912I,'Hernia','Estirpamiento de hernia',44, 'Cirugia')");
			int insercion9=smt.executeUpdate("insert into Pacientes values('Rodrigo','Dominguez',958912378,'89034567S,'Hernia','Estirpamiento de hernia',33, 'Cirugia')");
			int insercion10=smt.executeUpdate("insert into Pacientes values('Peter','Bronckhost',957080803,'49596956M,'Hernia','Estirpamiento de hernia',40, 'Cirugia')");
			int insercion11=smt.executeUpdate("insert into Pacientes values('Jorge','Marin',958765410,'12345693T,'Pulmonia','Encharcamiento de pulmones',29, 'Antibiotico')");
			int insercion12=smt.executeUpdate("insert into Pacientes values('Josefa','Sanchez',95789102,'45678912P,'Pulmonia','Encharcamiento de pulmones',60, null)");
			int insercion13=smt.executeUpdate("insert into Pacientes values('Pereira','Romero',95678910,'67801234J','Pulmonia','Encharcamiento de pulmones',45, 'Antibiotico')");
			int insercion14=smt.executeUpdate("insert into Pacientes values('Ana','Zamora',941267890,'89898124Z,'Pulmonia','Encharcamiento de pulmones',20, 'Antibiotico')");
			int insercion15=smt.executeUpdate("insert into Pacientes values('Sancho','Robinson',978910345,'56788012L','Resfriado','Con muchos mocos',5, 'Pastillas')");
			int insercion16=smt.executeUpdate("insert into Pacientes values('Andre','Segalerva',95367891,'89234568H','Resfriado','Con muchos mocos',34, 'Pastillas')");
			int insercion17=smt.executeUpdate("insert into Pacientes values('Ian','Villafranca',954789102,'67891234L','Resfriado','Con muchos mocos',12, 'Pastillas')");
			int insercion18=smt.executeUpdate("insert into Pacientes values('Maria','Dominguez',954678120,'12345679O','Resfriado','Con muchos mocos',20, 'Pastillas')");
			int insercion19=smt.executeUpdate("insert into Pacientes values('Pepe','Luis',952134567,'56348991A,'Resfriado','Con muchos mocos',12, 'Pastillas')");
			int insercion20=smt.executeUpdate("insert into Pacientes values('Pablo','Fernandez',956781234,'66670609W,'Resfriado','Con muchos mocos',90, null)");
			
			ResultSet rs=smt.executeQuery("select * from Pacientes");
			
			while(rs.next()){
				System.out.println(rs.getInt("Telefono")+ rs.getInt("Gravedad")+" : " + 
						rs.getString("Nombre")+rs.getString("Apellidos")+rs.getString("Dni")+rs.getString("Enfermedad")+rs.getString("Descripcion")+rs.getString("Tratamiento")); 
			}


		} catch (SQLException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();              
 

		}
		
	} 
	

}




